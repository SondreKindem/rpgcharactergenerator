CREATE TABLE [characters] (
  [ID] int PRIMARY KEY IDENTITY(1, 1),
  [name] nvarchar(255) NOT NULL,
  [hp] int NOT NULL,
  [mana] int NOT NULL,
  [strength] int NOT NULL,
  [intelligence] int NOT NULL,
  [dexterity] int NOT NULL,
  [armor_rating] int NOT NULL,
  [magic_resistance] int NOT NULL,
  [class] int NOT NULL
)
GO

CREATE TABLE [classes] (
  [ID] int PRIMARY KEY IDENTITY(1, 1),
  [name] nvarchar(255) NOT NULL,
  [qualified_name] nvarchar(255) NOT NULL
)
GO

ALTER TABLE [characters] ADD FOREIGN KEY ([class]) REFERENCES [classes] ([ID])
GO

INSERT INTO [classes] ([name], qualified_name) VALUES ('Arcane mage', 'ArcaneMage');
INSERT INTO [classes] ([name], qualified_name) VALUES ('Battlemage', 'BattleMage');
INSERT INTO [classes] ([name], qualified_name) VALUES ('Elemental mage', 'ElementalMage');
INSERT INTO [classes] ([name], qualified_name) VALUES ('Brawler', 'BrawlerFighter');
GO