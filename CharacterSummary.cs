﻿using RPGClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGCharacterGenerator
{
    /// <summary>
    /// Displays a summary of the created character
    /// and lets the user save the summary to a file
    /// </summary>
    public partial class CharacterSummary : Form
    {

        private Character character;
        private string summary;

        public CharacterSummary(Character character)
        {
            this.character = character;

            InitializeComponent();

            // Build the character summary
            StringBuilder summaryBuilder = new StringBuilder();
            summaryBuilder.AppendLine("CHARACTER SUMMARY:")
                .AppendLine("################")
                .Append("Name: ").AppendLine(character.Name)
                .Append("Class: ").AppendLine(character.GetType().Name)
                .Append("Equipped weapon: ").AppendLine(character.Weapon ?? "none")
                .Append("Health: ").AppendLine(character.Hp.ToString())
                .Append("Mana: ").AppendLine(character.Mana.ToString())
                .Append("Strength: ").AppendLine(character.Strength.ToString())
                .Append("Intelligence: ").AppendLine(character.Intelligence.ToString())
                .Append("Dexterity: ").AppendLine(character.Dexterity.ToString())
                .Append("Armor: ").AppendLine(character.ArmorRating.ToString())
                .Append("Magic resistance: ").AppendLine(character.MagicResistance.ToString())
                .Append("################");

            summary = summaryBuilder.ToString();
            SummaryTextBox.Text = summary;

            // Preset result to cancel
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Save the character summary to a text file
        /// </summary>
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
