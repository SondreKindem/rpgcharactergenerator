﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.SqlClient;
using RPGClasses;

namespace RPGCharacterGenerator
{
    public partial class Form1 : Form
    {
        private BindingList<Character> existingCharacters = new BindingList<Character>();
        // Kinda scuffed keeping type names in a collection, but at least it works
        private Dictionary<string, int> typeDict = new Dictionary<string, int>() { { "ArcaneMage", 0 }, { "BattleMage", 1 }, { "ElementalMage", 2 }, { "BrawlerFighter", 3 } };

        private SqlConnectionStringBuilder builder;

        public Form1()
        {
            InitializeComponent();
            builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7584\\SQLEXPRESS";
            builder.InitialCatalog = "RPGCharacters";
            builder.IntegratedSecurity = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // Load classes and characters
                foreach(string className in DBManager.GetInstance.GetClasses())
                    TypeComboBox.Items.Add(className);

                LoadCharacters();
                
            }
            catch (SqlException ex)
            {
                StatusText.Text = ex.Message;
            }
            
            // add the existing list to the CharacterList listbox
            CharacterList.DataSource = existingCharacters;
            CharacterList.DisplayMember = "Name";

            ReplaceBtnToolTip.SetToolTip(ReplaceBtn, "Replace selected character with current config");
        }

        /// <summary>
        /// Clear existing characters then fetch characters again
        /// </summary>
        private void ReloadCharacters()
        {
            existingCharacters.Clear();
            LoadCharacters();
        }

        /// <summary>
        /// Load characters into character list
        /// </summary>
        private void LoadCharacters()
        {
            // Adding manually to make the BindingList actually send update event to ui
            foreach(Character character in DBManager.GetInstance.GetCharacters())
            {
                existingCharacters.Add(character);
            }
        }

        /// <summary>
        /// Create the character, then show summary
        /// </summary>
        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Character newCharacter = CreateCharacterFromInput();

                // Show the summary form and check form result
                if(new CharacterSummary(newCharacter).ShowDialog(this) == DialogResult.OK)
                {
                    if (SaveCharacter(newCharacter))
                    {
                        StatusText.Text = "Saved character summary successfully";
                        ReloadCharacters();
                    }
                }
            }
            catch(Exception ex)
            {
                new ErrorDialog(ex.Message).ShowDialog(this); ;
            }
        }

        private bool SaveCharacter(Character character)
        {
            try
            {
                DBManager.GetInstance.CreateCharacter(character);
                return true;
            }
            catch (SqlException ex)
            {
                // Kinda lame to directly change text from multiple places in the code.
                // Would be better to set up a messaging system, but ain't nobody go time for that...
                StatusText.Text = ex.Message;
                return false;
            }
        }

        private Character CreateCharacterFromInput()
        {
            if (!ValidateCharacterForm())
            {
                throw new ArgumentException("Please fill in all character info!");
            }

            // spin boxes contain doubles, so we store all the values in vars to avoid converting to int multiple times
            int hp = (int)HPUpDown.Value;
            int mana = (int)ManaUpDown.Value;
            int strength = (int)StrengthUpDown.Value;
            int intelligence = (int)IntelligenceUpDown.Value;
            int dexterity = (int)DexterityUpDown.Value;
            double armorRating = (double)ArmorUpDown.Value;
            double magicRes = (double)MagicResUpDown.Value;

            Character newCharacter;
            switch (TypeComboBox.SelectedIndex)
            {
                case 0:
                    newCharacter = new ArcaneMage(NameTextBox.Text, hp, strength, intelligence, dexterity, armorRating, magicRes, mana);
                    break;
                case 1:
                    newCharacter = new BattleMage(NameTextBox.Text, hp, strength, intelligence, dexterity, armorRating, magicRes, mana);
                    break;
                case 2:
                    newCharacter = new ElementalMage(NameTextBox.Text, hp, strength, intelligence, dexterity, armorRating, magicRes, mana);
                    break;
                case 3:
                    newCharacter = new BrawlerFighter(NameTextBox.Text, hp, strength, intelligence, dexterity, armorRating, magicRes, mana);
                    break;
                case -1:
                default:
                    throw new ArgumentException("Character type not selected!");
            }

            return newCharacter;
        }

        /// <summary>
        /// Checks if the input fields contain information
        /// </summary>
        /// <returns>true if all input fields seem ok</returns>
        private bool ValidateCharacterForm()
        {
            if (
                TypeComboBox.SelectedIndex < 0 || 
                String.IsNullOrWhiteSpace(NameTextBox.Text)
                )
                return false;
            return true;
        }

        /// <summary>
        /// Provides all input fields with random stats
        /// </summary>
        private async void RandomizeBtn_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            SetControls(await GetName(), random.Next(1, 100), random.Next(1, 100), random.Next(1, 100), random.Next(1, 100), random.Next(1, 100), random.Next(1, 100), random.Next(1, 100), random.Next(0, 4));
        }

        /// <summary>
        /// Gets a random name from a name generator api
        /// </summary>
        /// <returns>A random name as a string</returns>
        private async Task<string> GetName()
        {
            using(var client = new HttpClient())
            {
                var result = await client.GetAsync(@"http://names.drycodes.com/1");
                string value = await result.Content.ReadAsStringAsync();
                // Can't be bothered to install json nuget package. Serialization is built-in in core 3
                return value.Replace("[", "").Replace("]", "").Replace("_", " ").Replace("\"", "");
            }
        }        

        /// <summary>
        /// Set the value of all controls
        /// </summary>
        private void SetControls(string name, int mana, int hp, int strength, int intelligence, int dexterity, double armor, double magicRes, int type)
        {
            HPUpDown.Value = hp;
            ManaUpDown.Value = mana;
            StrengthUpDown.Value = strength;
            IntelligenceUpDown.Value = intelligence;
            DexterityUpDown.Value = dexterity;
            ArmorUpDown.Value = (int)armor;
            MagicResUpDown.Value = (int)magicRes;

            TypeComboBox.SelectedIndex = type;

            NameTextBox.Text = name;
        }

        /// <summary>
        /// Reset all controls
        /// </summary>
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            SetControls("", 0, 0, 0, 0, 0, 0, 0, -1);
        }

        /// <summary>
        /// Retrieve the selected character into the controls
        /// </summary>
        private void LoadCharBtn_Click(object sender, EventArgs e)
        {
            Character character = (Character)CharacterList.SelectedItem;
            SetControls(character.Name, character.Mana, character.Hp, character.Strength, character.Intelligence, character.Dexterity, character.ArmorRating, character.MagicResistance, typeDict[character.GetType().Name]);
        }

        /// <summary>
        /// Remove the currently selected character from the character list
        /// </summary>
        private void DeleteCharBtn_Click(object sender, EventArgs e)
        {
            //existingCharacters.Remove((Character)CharacterList.SelectedItem);
            if (DBManager.GetInstance.DeleteCharacter(CharacterList.SelectedIndex))
            {
                ReloadCharacters();
                StatusText.Text = "Character deleted";
            }
            else
            {
                StatusText.Text = "Could not delete character";
            }
            
        }

        /// <summary>
        /// Replace the selected character from the character list with the current input values
        /// </summary>
        private void ReplaceBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // Bug: some classes get a boost in stats when created. When replacing those stats get boosted again.
                Character editedCharacter = CreateCharacterFromInput();
                if(DBManager.GetInstance.UpdateCharacter(CharacterList.SelectedIndex, editedCharacter))
                {
                    StatusText.Text = "Updated character " + editedCharacter.Name;
                    ReloadCharacters();
                }
                else
                    StatusText.Text = "Error when trying to update character";
            }
            catch (Exception ex)
            {
                new ErrorDialog(ex.Message).ShowDialog(this); ;
            }
        }
    }
}
