﻿namespace RPGCharacterGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainWrap = new System.Windows.Forms.TableLayoutPanel();
            this.topLayout = new System.Windows.Forms.TableLayoutPanel();
            this.CharacterCreationGroup = new System.Windows.Forms.GroupBox();
            this.CharacterForm = new System.Windows.Forms.TableLayoutPanel();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.TypeComboBox = new System.Windows.Forms.ComboBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.HPLabel = new System.Windows.Forms.Label();
            this.HPUpDown = new System.Windows.Forms.NumericUpDown();
            this.ManaLabel = new System.Windows.Forms.Label();
            this.ManaUpDown = new System.Windows.Forms.NumericUpDown();
            this.StrengthLabel = new System.Windows.Forms.Label();
            this.StrengthUpDown = new System.Windows.Forms.NumericUpDown();
            this.IntelligenceLabel = new System.Windows.Forms.Label();
            this.IntelligenceUpDown = new System.Windows.Forms.NumericUpDown();
            this.DexterityLabel = new System.Windows.Forms.Label();
            this.DexterityUpDown = new System.Windows.Forms.NumericUpDown();
            this.ArmorLabel = new System.Windows.Forms.Label();
            this.ArmorUpDown = new System.Windows.Forms.NumericUpDown();
            this.MagicResLabel = new System.Windows.Forms.Label();
            this.MagicResUpDown = new System.Windows.Forms.NumericUpDown();
            this.RandomizeBtn = new System.Windows.Forms.Button();
            this.CharacterListWrap = new System.Windows.Forms.TableLayoutPanel();
            this.CharacterList = new System.Windows.Forms.ListBox();
            this.CharacterListOptions = new System.Windows.Forms.TableLayoutPanel();
            this.DeleteCharBtn = new System.Windows.Forms.Button();
            this.LoadCharBtn = new System.Windows.Forms.Button();
            this.CharacterListLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.StatusText = new System.Windows.Forms.Label();
            this.ReplaceBtn = new System.Windows.Forms.Button();
            this.ReplaceBtnToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainWrap.SuspendLayout();
            this.topLayout.SuspendLayout();
            this.CharacterCreationGroup.SuspendLayout();
            this.CharacterForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HPUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManaUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrengthUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntelligenceUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DexterityUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArmorUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MagicResUpDown)).BeginInit();
            this.CharacterListWrap.SuspendLayout();
            this.CharacterListOptions.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainWrap
            // 
            this.mainWrap.ColumnCount = 1;
            this.mainWrap.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainWrap.Controls.Add(this.topLayout, 0, 0);
            this.mainWrap.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.mainWrap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainWrap.Location = new System.Drawing.Point(0, 0);
            this.mainWrap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainWrap.Name = "mainWrap";
            this.mainWrap.RowCount = 2;
            this.mainWrap.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainWrap.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.mainWrap.Size = new System.Drawing.Size(492, 411);
            this.mainWrap.TabIndex = 0;
            // 
            // topLayout
            // 
            this.topLayout.ColumnCount = 2;
            this.topLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.topLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.topLayout.Controls.Add(this.CharacterCreationGroup, 1, 1);
            this.topLayout.Controls.Add(this.CharacterListWrap, 0, 1);
            this.topLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topLayout.Location = new System.Drawing.Point(3, 2);
            this.topLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.topLayout.Name = "topLayout";
            this.topLayout.RowCount = 2;
            this.topLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.topLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.topLayout.Size = new System.Drawing.Size(486, 365);
            this.topLayout.TabIndex = 1;
            // 
            // CharacterCreationGroup
            // 
            this.CharacterCreationGroup.Controls.Add(this.CharacterForm);
            this.CharacterCreationGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharacterCreationGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CharacterCreationGroup.Location = new System.Drawing.Point(171, 2);
            this.CharacterCreationGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterCreationGroup.Name = "CharacterCreationGroup";
            this.CharacterCreationGroup.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterCreationGroup.Size = new System.Drawing.Size(312, 361);
            this.CharacterCreationGroup.TabIndex = 3;
            this.CharacterCreationGroup.TabStop = false;
            this.CharacterCreationGroup.Text = "Character Creation";
            // 
            // CharacterForm
            // 
            this.CharacterForm.ColumnCount = 2;
            this.CharacterForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.CharacterForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 625F));
            this.CharacterForm.Controls.Add(this.TypeLabel, 0, 0);
            this.CharacterForm.Controls.Add(this.TypeComboBox, 1, 0);
            this.CharacterForm.Controls.Add(this.NameLabel, 0, 1);
            this.CharacterForm.Controls.Add(this.NameTextBox, 1, 1);
            this.CharacterForm.Controls.Add(this.HPLabel, 0, 2);
            this.CharacterForm.Controls.Add(this.HPUpDown, 1, 2);
            this.CharacterForm.Controls.Add(this.ManaLabel, 0, 3);
            this.CharacterForm.Controls.Add(this.ManaUpDown, 1, 3);
            this.CharacterForm.Controls.Add(this.StrengthLabel, 0, 4);
            this.CharacterForm.Controls.Add(this.StrengthUpDown, 1, 4);
            this.CharacterForm.Controls.Add(this.IntelligenceLabel, 0, 5);
            this.CharacterForm.Controls.Add(this.IntelligenceUpDown, 1, 5);
            this.CharacterForm.Controls.Add(this.DexterityLabel, 0, 6);
            this.CharacterForm.Controls.Add(this.DexterityUpDown, 1, 6);
            this.CharacterForm.Controls.Add(this.ArmorLabel, 0, 7);
            this.CharacterForm.Controls.Add(this.ArmorUpDown, 1, 7);
            this.CharacterForm.Controls.Add(this.MagicResLabel, 0, 8);
            this.CharacterForm.Controls.Add(this.MagicResUpDown, 1, 8);
            this.CharacterForm.Controls.Add(this.RandomizeBtn, 1, 9);
            this.CharacterForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharacterForm.Location = new System.Drawing.Point(3, 17);
            this.CharacterForm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterForm.Name = "CharacterForm";
            this.CharacterForm.RowCount = 10;
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CharacterForm.Size = new System.Drawing.Size(306, 342);
            this.CharacterForm.TabIndex = 2;
            // 
            // TypeLabel
            // 
            this.TypeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TypeLabel.AutoSize = true;
            this.TypeLabel.Location = new System.Drawing.Point(13, 0);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(69, 17);
            this.TypeLabel.TabIndex = 7;
            this.TypeLabel.Text = "Char type";
            // 
            // TypeComboBox
            // 
            this.TypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypeComboBox.FormattingEnabled = true;
            this.TypeComboBox.Location = new System.Drawing.Point(88, 2);
            this.TypeComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TypeComboBox.Name = "TypeComboBox";
            this.TypeComboBox.Size = new System.Drawing.Size(203, 24);
            this.TypeComboBox.TabIndex = 0;
            // 
            // NameLabel
            // 
            this.NameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(37, 28);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(45, 17);
            this.NameLabel.TabIndex = 3;
            this.NameLabel.Text = "Name";
            this.NameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(88, 30);
            this.NameTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(203, 22);
            this.NameTextBox.TabIndex = 0;
            // 
            // HPLabel
            // 
            this.HPLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HPLabel.AutoSize = true;
            this.HPLabel.Location = new System.Drawing.Point(55, 54);
            this.HPLabel.Name = "HPLabel";
            this.HPLabel.Size = new System.Drawing.Size(27, 17);
            this.HPLabel.TabIndex = 4;
            this.HPLabel.Text = "HP";
            // 
            // HPUpDown
            // 
            this.HPUpDown.Location = new System.Drawing.Point(88, 56);
            this.HPUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HPUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.HPUpDown.Name = "HPUpDown";
            this.HPUpDown.Size = new System.Drawing.Size(120, 22);
            this.HPUpDown.TabIndex = 21;
            // 
            // ManaLabel
            // 
            this.ManaLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ManaLabel.AutoSize = true;
            this.ManaLabel.Location = new System.Drawing.Point(39, 80);
            this.ManaLabel.Name = "ManaLabel";
            this.ManaLabel.Size = new System.Drawing.Size(43, 17);
            this.ManaLabel.TabIndex = 5;
            this.ManaLabel.Text = "Mana";
            // 
            // ManaUpDown
            // 
            this.ManaUpDown.Location = new System.Drawing.Point(88, 82);
            this.ManaUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ManaUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.ManaUpDown.Name = "ManaUpDown";
            this.ManaUpDown.Size = new System.Drawing.Size(120, 22);
            this.ManaUpDown.TabIndex = 20;
            // 
            // StrengthLabel
            // 
            this.StrengthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StrengthLabel.AutoSize = true;
            this.StrengthLabel.Location = new System.Drawing.Point(20, 106);
            this.StrengthLabel.Name = "StrengthLabel";
            this.StrengthLabel.Size = new System.Drawing.Size(62, 17);
            this.StrengthLabel.TabIndex = 8;
            this.StrengthLabel.Text = "Strength";
            // 
            // StrengthUpDown
            // 
            this.StrengthUpDown.Location = new System.Drawing.Point(88, 108);
            this.StrengthUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StrengthUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.StrengthUpDown.Name = "StrengthUpDown";
            this.StrengthUpDown.Size = new System.Drawing.Size(120, 22);
            this.StrengthUpDown.TabIndex = 19;
            // 
            // IntelligenceLabel
            // 
            this.IntelligenceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IntelligenceLabel.AutoSize = true;
            this.IntelligenceLabel.Location = new System.Drawing.Point(3, 132);
            this.IntelligenceLabel.Name = "IntelligenceLabel";
            this.IntelligenceLabel.Size = new System.Drawing.Size(79, 17);
            this.IntelligenceLabel.TabIndex = 9;
            this.IntelligenceLabel.Text = "Intelligence";
            // 
            // IntelligenceUpDown
            // 
            this.IntelligenceUpDown.Location = new System.Drawing.Point(88, 134);
            this.IntelligenceUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.IntelligenceUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.IntelligenceUpDown.Name = "IntelligenceUpDown";
            this.IntelligenceUpDown.Size = new System.Drawing.Size(120, 22);
            this.IntelligenceUpDown.TabIndex = 18;
            // 
            // DexterityLabel
            // 
            this.DexterityLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DexterityLabel.AutoSize = true;
            this.DexterityLabel.Location = new System.Drawing.Point(19, 158);
            this.DexterityLabel.Name = "DexterityLabel";
            this.DexterityLabel.Size = new System.Drawing.Size(63, 17);
            this.DexterityLabel.TabIndex = 10;
            this.DexterityLabel.Text = "Dexterity";
            // 
            // DexterityUpDown
            // 
            this.DexterityUpDown.Location = new System.Drawing.Point(88, 160);
            this.DexterityUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DexterityUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.DexterityUpDown.Name = "DexterityUpDown";
            this.DexterityUpDown.Size = new System.Drawing.Size(120, 22);
            this.DexterityUpDown.TabIndex = 17;
            // 
            // ArmorLabel
            // 
            this.ArmorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ArmorLabel.AutoSize = true;
            this.ArmorLabel.Location = new System.Drawing.Point(36, 184);
            this.ArmorLabel.Name = "ArmorLabel";
            this.ArmorLabel.Size = new System.Drawing.Size(46, 17);
            this.ArmorLabel.TabIndex = 14;
            this.ArmorLabel.Text = "Armor";
            // 
            // ArmorUpDown
            // 
            this.ArmorUpDown.Location = new System.Drawing.Point(88, 186);
            this.ArmorUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ArmorUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.ArmorUpDown.Name = "ArmorUpDown";
            this.ArmorUpDown.Size = new System.Drawing.Size(120, 22);
            this.ArmorUpDown.TabIndex = 16;
            // 
            // MagicResLabel
            // 
            this.MagicResLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MagicResLabel.AutoSize = true;
            this.MagicResLabel.Location = new System.Drawing.Point(12, 210);
            this.MagicResLabel.Name = "MagicResLabel";
            this.MagicResLabel.Size = new System.Drawing.Size(70, 17);
            this.MagicResLabel.TabIndex = 15;
            this.MagicResLabel.Text = "MagicRes";
            // 
            // MagicResUpDown
            // 
            this.MagicResUpDown.Location = new System.Drawing.Point(88, 212);
            this.MagicResUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MagicResUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.MagicResUpDown.Name = "MagicResUpDown";
            this.MagicResUpDown.Size = new System.Drawing.Size(120, 22);
            this.MagicResUpDown.TabIndex = 22;
            // 
            // RandomizeBtn
            // 
            this.RandomizeBtn.AutoSize = true;
            this.RandomizeBtn.Location = new System.Drawing.Point(88, 238);
            this.RandomizeBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RandomizeBtn.Name = "RandomizeBtn";
            this.RandomizeBtn.Size = new System.Drawing.Size(119, 34);
            this.RandomizeBtn.TabIndex = 2;
            this.RandomizeBtn.Text = "Randomize";
            this.RandomizeBtn.UseVisualStyleBackColor = true;
            this.RandomizeBtn.Click += new System.EventHandler(this.RandomizeBtn_Click);
            // 
            // CharacterListWrap
            // 
            this.CharacterListWrap.ColumnCount = 1;
            this.CharacterListWrap.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.CharacterListWrap.Controls.Add(this.CharacterList, 0, 1);
            this.CharacterListWrap.Controls.Add(this.CharacterListOptions, 0, 2);
            this.CharacterListWrap.Controls.Add(this.CharacterListLabel, 0, 0);
            this.CharacterListWrap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharacterListWrap.Location = new System.Drawing.Point(3, 2);
            this.CharacterListWrap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterListWrap.Name = "CharacterListWrap";
            this.CharacterListWrap.RowCount = 3;
            this.CharacterListWrap.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.CharacterListWrap.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.4434F));
            this.CharacterListWrap.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.5566F));
            this.CharacterListWrap.Size = new System.Drawing.Size(162, 361);
            this.CharacterListWrap.TabIndex = 4;
            // 
            // CharacterList
            // 
            this.CharacterList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharacterList.FormattingEnabled = true;
            this.CharacterList.ItemHeight = 16;
            this.CharacterList.Location = new System.Drawing.Point(3, 22);
            this.CharacterList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterList.Name = "CharacterList";
            this.CharacterList.Size = new System.Drawing.Size(156, 297);
            this.CharacterList.TabIndex = 2;
            // 
            // CharacterListOptions
            // 
            this.CharacterListOptions.ColumnCount = 2;
            this.CharacterListOptions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CharacterListOptions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CharacterListOptions.Controls.Add(this.DeleteCharBtn, 0, 0);
            this.CharacterListOptions.Controls.Add(this.LoadCharBtn, 1, 0);
            this.CharacterListOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CharacterListOptions.Location = new System.Drawing.Point(3, 323);
            this.CharacterListOptions.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CharacterListOptions.Name = "CharacterListOptions";
            this.CharacterListOptions.RowCount = 1;
            this.CharacterListOptions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CharacterListOptions.Size = new System.Drawing.Size(156, 36);
            this.CharacterListOptions.TabIndex = 3;
            // 
            // DeleteCharBtn
            // 
            this.DeleteCharBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeleteCharBtn.Location = new System.Drawing.Point(3, 2);
            this.DeleteCharBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DeleteCharBtn.Name = "DeleteCharBtn";
            this.DeleteCharBtn.Size = new System.Drawing.Size(72, 32);
            this.DeleteCharBtn.TabIndex = 0;
            this.DeleteCharBtn.Text = "Delete";
            this.DeleteCharBtn.UseVisualStyleBackColor = true;
            this.DeleteCharBtn.Click += new System.EventHandler(this.DeleteCharBtn_Click);
            // 
            // LoadCharBtn
            // 
            this.LoadCharBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadCharBtn.Location = new System.Drawing.Point(81, 2);
            this.LoadCharBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LoadCharBtn.Name = "LoadCharBtn";
            this.LoadCharBtn.Size = new System.Drawing.Size(72, 32);
            this.LoadCharBtn.TabIndex = 1;
            this.LoadCharBtn.Text = "Load";
            this.LoadCharBtn.UseVisualStyleBackColor = true;
            this.LoadCharBtn.Click += new System.EventHandler(this.LoadCharBtn_Click);
            // 
            // CharacterListLabel
            // 
            this.CharacterListLabel.AutoSize = true;
            this.CharacterListLabel.Location = new System.Drawing.Point(3, 0);
            this.CharacterListLabel.Name = "CharacterListLabel";
            this.CharacterListLabel.Size = new System.Drawing.Size(119, 17);
            this.CharacterListLabel.TabIndex = 4;
            this.CharacterListLabel.Text = "Saved characters";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.Controls.Add(this.ClearBtn, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.SubmitBtn, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.ReplaceBtn, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.StatusText, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 371);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(486, 38);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // ClearBtn
            // 
            this.ClearBtn.AutoSize = true;
            this.ClearBtn.Location = new System.Drawing.Point(324, 2);
            this.ClearBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(68, 32);
            this.ClearBtn.TabIndex = 1;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubmitBtn.AutoSize = true;
            this.SubmitBtn.Location = new System.Drawing.Point(402, 2);
            this.SubmitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(81, 32);
            this.SubmitBtn.TabIndex = 0;
            this.SubmitBtn.Text = "Submit";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // StatusText
            // 
            this.StatusText.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.StatusText.AutoSize = true;
            this.StatusText.Location = new System.Drawing.Point(97, 10);
            this.StatusText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusText.Name = "StatusText";
            this.StatusText.Size = new System.Drawing.Size(0, 17);
            this.StatusText.TabIndex = 2;
            // 
            // ReplaceBtn
            // 
            this.ReplaceBtn.AutoSize = true;
            this.ReplaceBtn.Location = new System.Drawing.Point(3, 3);
            this.ReplaceBtn.Name = "ReplaceBtn";
            this.ReplaceBtn.Size = new System.Drawing.Size(87, 32);
            this.ReplaceBtn.TabIndex = 3;
            this.ReplaceBtn.Text = "Replace";
            this.ReplaceBtn.UseVisualStyleBackColor = true;
            this.ReplaceBtn.Click += new System.EventHandler(this.ReplaceBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 411);
            this.Controls.Add(this.mainWrap);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(499, 448);
            this.Name = "Form1";
            this.Text = "RPG Character Creator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mainWrap.ResumeLayout(false);
            this.topLayout.ResumeLayout(false);
            this.CharacterCreationGroup.ResumeLayout(false);
            this.CharacterForm.ResumeLayout(false);
            this.CharacterForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HPUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManaUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrengthUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntelligenceUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DexterityUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArmorUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MagicResUpDown)).EndInit();
            this.CharacterListWrap.ResumeLayout(false);
            this.CharacterListWrap.PerformLayout();
            this.CharacterListOptions.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainWrap;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.TableLayoutPanel topLayout;
        private System.Windows.Forms.ComboBox TypeComboBox;
        private System.Windows.Forms.ListBox CharacterList;
        private System.Windows.Forms.GroupBox CharacterCreationGroup;
        private System.Windows.Forms.TableLayoutPanel CharacterForm;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label HPLabel;
        private System.Windows.Forms.Label ManaLabel;
        private System.Windows.Forms.Label TypeLabel;
        private System.Windows.Forms.Label StrengthLabel;
        private System.Windows.Forms.Label IntelligenceLabel;
        private System.Windows.Forms.Label DexterityLabel;
        private System.Windows.Forms.Label ArmorLabel;
        private System.Windows.Forms.Label MagicResLabel;
        private System.Windows.Forms.NumericUpDown ArmorUpDown;
        private System.Windows.Forms.NumericUpDown DexterityUpDown;
        private System.Windows.Forms.NumericUpDown IntelligenceUpDown;
        private System.Windows.Forms.NumericUpDown StrengthUpDown;
        private System.Windows.Forms.NumericUpDown ManaUpDown;
        private System.Windows.Forms.NumericUpDown HPUpDown;
        private System.Windows.Forms.NumericUpDown MagicResUpDown;
        private System.Windows.Forms.TableLayoutPanel CharacterListWrap;
        private System.Windows.Forms.TableLayoutPanel CharacterListOptions;
        private System.Windows.Forms.Button DeleteCharBtn;
        private System.Windows.Forms.Button LoadCharBtn;
        private System.Windows.Forms.Label CharacterListLabel;
        private System.Windows.Forms.Button RandomizeBtn;
        private System.Windows.Forms.Label StatusText;
        private System.Windows.Forms.Button ReplaceBtn;
        private System.Windows.Forms.ToolTip ReplaceBtnToolTip;
    }
}

