﻿using Microsoft.Data.SqlClient;
using RPGClasses;
using System;
using System.Collections.Generic;

namespace RPGCharacterGenerator
{
    /// <summary>
    /// A singleton that connects to the db and handles all transactions
    /// </summary>
    public sealed class DBManager
    {
        private static DBManager instance = null;
        private SqlConnectionStringBuilder builder;
        // I want to keep track of the id for each character. This sucks, but works (I hope)
        private Dictionary<int, int> idTracker = new Dictionary<int, int>();

        public static DBManager GetInstance
        {
            get
            {
                if(instance == null)
                    instance = new DBManager();
                return instance;
            }
        }

        private DBManager()
        {
            builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7584\\SQLEXPRESS";
            builder.InitialCatalog = "RPGCharacters";
            builder.IntegratedSecurity = true;
        }

        /// <summary>
        /// Get all characters from the db
        /// </summary>
        /// <returns>A list of all characters</returns>
        public List<Character> GetCharacters()
        {
            List<Character> characterList = new List<Character>();
            
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                using (SqlCommand command = new SqlCommand("SELECT * FROM characters INNER JOIN classes ON characters.class = classes.ID", connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Clear the internal id tracker, we are rebuilding the character list
                        idTracker.Clear();

                        while (reader.Read())
                        {
                            idTracker.Add(characterList.Count, reader.GetInt32(reader.GetOrdinal("ID")));

                            // Get class from db record and use reflection to instantiate
                            characterList.Add((Character)Activator.CreateInstance(
                                Type.GetType($"RPGClasses.{reader.GetString(reader.GetOrdinal("qualified_name"))}, RPGClasses"),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.GetInt32(reader.GetOrdinal("hp")),
                                reader.GetInt32(reader.GetOrdinal("strength")),
                                reader.GetInt32(reader.GetOrdinal("intelligence")),
                                reader.GetInt32(reader.GetOrdinal("dexterity")),
                                reader.GetInt32(reader.GetOrdinal("armor_rating")),
                                reader.GetInt32(reader.GetOrdinal("magic_resistance")),
                                reader.GetInt32(reader.GetOrdinal("mana"))));
                        }
                    }
                }
            }
            catch (SqlException) {}
            
            return characterList;
        }

        public List<string> GetClasses()
        {
            List<string> classList = new List<string>();

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("SELECT name FROM classes", connection))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        classList.Add(reader.GetString(0));
                    }
                }
            }

            return classList;
        }

        public bool UpdateCharacter(int index, Character character)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Build cmd string, getting class id with a subquery on the class qualified_name
                    string cmd = "UPDATE characters SET name = @name, hp = @hp, mana = @mana, strength = @strength, intelligence =  @int, dexterity = @dex, armor_rating = @armor, magic_resistance = @magic, class = (SELECT ID FROM classes WHERE qualified_name = @class) WHERE ID = @id";
                    using (SqlCommand command = new SqlCommand(cmd, connection))
                    {
                        command.Parameters.AddWithValue("@name", character.Name);
                        command.Parameters.AddWithValue("@hp", character.Hp);
                        command.Parameters.AddWithValue("@mana", character.Mana);
                        command.Parameters.AddWithValue("@strength", character.Strength);
                        command.Parameters.AddWithValue("@int", character.Intelligence);
                        command.Parameters.AddWithValue("@dex", character.Dexterity);
                        command.Parameters.AddWithValue("@armor", character.ArmorRating);
                        command.Parameters.AddWithValue("@magic", character.MagicResistance);
                        command.Parameters.AddWithValue("@class", character.GetType().Name);
                        command.Parameters.AddWithValue("@id", idTracker[index]);
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public bool CreateCharacter(Character character)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Build cmd string, getting class id with a subquery on the class qualified_name
                    string cmd = "INSERT INTO characters (name, hp, mana, strength, intelligence, dexterity, armor_rating, magic_resistance, class) VALUES (@name, @hp, @mana, @strength, @int, @dex, @armor, @magic, (SELECT ID FROM classes WHERE qualified_name = @class))";
                    using (SqlCommand command = new SqlCommand(cmd, connection))
                    {
                        command.Parameters.AddWithValue("@name", character.Name);
                        command.Parameters.AddWithValue("@hp", character.Hp);
                        command.Parameters.AddWithValue("@mana", character.Mana);
                        command.Parameters.AddWithValue("@strength", character.Strength);
                        command.Parameters.AddWithValue("@int", character.Intelligence);
                        command.Parameters.AddWithValue("@dex", character.Dexterity);
                        command.Parameters.AddWithValue("@armor", character.ArmorRating);
                        command.Parameters.AddWithValue("@magic", character.MagicResistance);
                        command.Parameters.AddWithValue("@class", character.GetType().Name);
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete a character at a given index. The index is the list
        /// </summary>
        /// <returns>true if deletion worked</returns>
        public bool DeleteCharacter(int index)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("DELETE FROM characters WHERE ID = @id", connection))
                    {
                        command.Parameters.AddWithValue("@id", idTracker[index]);
                        command.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (SqlException)
            {
                return false;
            }
        }
    }
}
